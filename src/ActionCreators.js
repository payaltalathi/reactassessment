import {ON_CLOSE_MODAL , ON_CLICK_EVENT} from './Actions';

export function OnCloseModal() {
  return { type: ON_CLOSE_MODAL  };
}

export function onClickEventHandle() {
    return { type : ON_CLICK_EVENT };
}  
