import {ON_CLOSE_MODAL , ON_CLICK_EVENT} from './Actions';



const initialState ={
    isOpen: false,
    isSelected: false,
    style:{backgroundColor:''}
}
const rootReducer = (state= initialState, action) =>{
    switch(action.type){
        case ON_CLOSE_MODAL:
            return {
                isSelected: false,
                isOpen: false,
                style:{backgroundColor:''},
            
            }


        case ON_CLICK_EVENT:
            return {
                isSelected: true,
                isOpen: true,
                style:{backgroundColor:'coral'},
            }
        
        
        default:
            return state
        }
}

export default rootReducer;