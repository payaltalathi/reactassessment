import React from 'react';
import Truncate from '../../node_modules/react-truncate';
import styled from '../../node_modules/styled-components';
import { string, shape } from '../../node_modules/prop-types';
import EmpInfoModal from './EmpInfoModal'
import { connect } from '../../node_modules/react-redux'
import { onClickEventHandle } from './ActionCreators'


const Wrapper = styled.div`
width: 30%;
border: 2px solid #333;
border-radius: 4px;
margin-bottom: 25px;
padding-right: 0.8px;
overflow: hidden;
text-decoration: none;
color: black;
`;


const Image = styled.img`
width: 42%;
float: left;
margin-right: 10px;
`;



const EmployeeCard =({ isOpen, isSelected,empCard, handleOnClickEvent })=>{
      

        const style={
            backgroundColor : isSelected ? 'coral' : ''
          }
      //    let style = {
      //       'outline-style': '',
      //       'outline-color': ''
      //  };
      //    if (isSelected) {
      //      style = {
      //       'outline-style': 'solid',
      //       'outline-color': 'coral'
      //      };
      //   } 
       return(
    <Wrapper className="employee-card"  onClick= {()=> handleOnClickEvent()} style={style} >
      <Image alt={empCard.firstName} src={empCard.avatar} />
      
      <div>
        <h3>{`${empCard.firstName} ${empCard.lastName}`}</h3>
        <p><Truncate lines={1}>{empCard.bio}</Truncate></p>
      </div>
      <EmpInfoModal open ={isOpen}  empInfo = {empCard}/>
    </Wrapper>
    

      ) };
   

       EmployeeCard.propTypes = {
         isSelected: Boolean.isRequired,
         isOpen: Boolean.isRequired, 
         handleOnClickEvent: Function.isRequired, 
         empCard:shape({
        firstName: string.isRequired,
        lastName: string.isRequired,
        avatar: string.isRequired,
        bio: string.isRequired
           }).isRequired
      };
      const mapStateToProps = state => ({
        isSelected: state.isSelected,
        isOpen: state.isOpen,
      });
    const mapDispatchToProps = (dispatch) =>({
        handleOnClickEvent(){
          dispatch(onClickEventHandle())
    
        }    })  
      
      
export default connect(mapStateToProps, mapDispatchToProps) (EmployeeCard);       
