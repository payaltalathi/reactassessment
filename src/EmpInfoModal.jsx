import React from 'react';
// import Modal from '../../node_modules/react-responsive-modal';
import Moment from '../../node_modules/react-moment';
import Modal from '../../node_modules/react-bootstrap-modal';
import { string, shape } from '../../node_modules/prop-types';
 
 const EmpInfoModal= (props)=>(
      
      <div>
        <Modal show={props.open} onHide={props.onClose} >
          <Modal.Header closeButton/>
          <Modal.Body>
                <img alt={props.empInfo.firstName} src={props.empInfo.avatar} />
                <h3>{`${props.empInfo.firstName} ${props.empInfo.lastName}`}</h3>
                <h6>{props.empInfo.jobTitle}</h6>
                <h6>{props.empInfo.age}</h6>
                <h6><Moment format="DD/MM/YYYY">{props.empInfo.dateJoined}</Moment></h6>
                <p>{props.empInfo.bio}</p>
          </Modal.Body>
             
         <Modal.Footer />
        </Modal>
     </div>
    );
 
    EmpInfoModal.propTypes = {
        open: Boolean.isRequired,
        onClose: Function.isRequired,
        empInfo:shape({
            firstName: string.isRequired,
            lastName: string.isRequired,
            avatar: string.isRequired,
            bio: string.isRequired
               }).isRequired

    }
        

  export default EmpInfoModal;