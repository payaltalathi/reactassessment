import React from 'react';
import ReactDOM from 'react-dom';
import FirstComponent from './FirstComponent';
import { Provider } from '../../node_modules/react-redux';
import { createStore, compose } from '../../node_modules/redux';
import rootReducer from './reducers'

const store = createStore(rootReducer,{isSelected: false, isOpen : false},compose(
  typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
));

ReactDOM.render(
    <Provider store={store}>
      <FirstComponent /> 
    </Provider>  
, document.getElementById('app'));
