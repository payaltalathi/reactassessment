import React from 'react';
import preload from '../sample-data.json';
import Moment from '../../node_modules/react-moment';
import { Flex, Box } from '../../node_modules/@rebass/grid';
import EmployeeCard from './EmployeeCard'


const Search = () => (
<Flex>  
  <Box className="header">
     <header> <h3>{preload.companyInfo.companyName}</h3>
     <h4>{preload.companyInfo.companyMotto}  <span className ="since">Since <Moment format="DD/MM/YYYY">{preload.companyInfo.companyEst}</Moment></span></h4>
     </header>
     <hr/>
  </Box>
    <Box>
     <h4 className = "emp">Our Employees</h4>
     <span className = "sortBy" >Sort by: <select><option value ="firstName">first name</option></select></span><span className = "searchFilter">Search <input type = "text" /></span>
     <hr1/>

     </Box>
  <Box className="search">
    <Box>

      {preload.employees.map(employee => <EmployeeCard empCard={employee}  key={employee.id}  />)}
    </Box>
  </Box>  
</Flex>  
);

export default Search;
